<?php get_header(); ?>

<?php get_template_part('head'); ?>

<?php get_template_part('topnav'); ?>

    <!-- start content container -->
    <div class="row ssrap-content">

        <?php //left sidebar ?>
        <?php get_sidebar( 'left' ); ?>

        <div class="col-md-<?php samstrap_main_content_width(); ?> ssrap-main">
         <h1><?php _e('Sorry This Page Does Not Exist!','samstrap'); ?></h1>
        </div>

        <?php //get the right sidebar ?>
        <?php get_sidebar( 'right' ); ?>

    </div>
    <!-- end content container -->

<?php get_footer(); ?>