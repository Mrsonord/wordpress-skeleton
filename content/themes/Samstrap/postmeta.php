<?php global $samstrap_settings; ?>
<?php if ($samstrap_settings['show_postmeta'] != 0) : ?>
    <p class="text-right">
        <span class="fa fa-user"></span> <?php the_author_posts_link(); ?>
        <span class="fa fa-time"></span> <?php the_time('F jS, Y'); ?>
        <span class="fa fa-edit"></span> <?php edit_post_link(__('Edit','samstrap')); ?>
    </p>
    <p class="text-right"><span class="fa fa-circle-arrow-right"></span> <?php _e('Posted In','samstrap'); ?>: <?php the_category(', '); ?></p>
    <?php if( has_tag() ) : ?>
        <p class="text-right"><span class="fa fa-tags"></span>
        <?php the_tags(); ?>
        </p>
    <?php endif; ?>
<?php endif; ?>