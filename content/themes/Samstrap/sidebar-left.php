<?php
    global $samstrap_settings;
    if ($samstrap_settings['left_sidebar'] != 0) : ?>
    <div class="col-md-<?php echo $samstrap_settings['left_sidebar_width']; ?> ssrap-left">
        <?php dynamic_sidebar( 'Left Sidebar' ); ?>
    </div>
<?php endif; ?>