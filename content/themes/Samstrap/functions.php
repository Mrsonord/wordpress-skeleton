<?php
////////////////////////////////////////////////////////////////////
// Theme Information
////////////////////////////////////////////////////////////////////

    $themename = "Samstrap";
    $developer_uri = "http://5am1am.com";
    $shortname = "ss";
    $version = '1.3';
    //load_theme_textdomain( 'samstrap', get_template_directory() . '/languages' );

////////////////////////////////////////////////////////////////////
// include Theme-options.php for Samstrap Theme settings
////////////////////////////////////////////////////////////////////

include 'theme-options.php';

function sams_scripts() {

	wp_deregister_script( 'jquery' );

	wp_register_script( 'jquery', '//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js', false, '2.0.3', true );
	wp_register_script( 'bootstrap', '//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js', array( 'jquery' ), '3.1.0', true );


	wp_register_style( 'font-awesome', '//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css', false, '4.0.3', false );
	//wp_register_style( 'bootstrap', '//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css', false, '3.1.0', false );
	wp_register_style( 'cyborg', '//netdna.bootstrapcdn.com/bootswatch/3.0.3/cyborg/bootstrap.min.css', false, '3.1.0', false);

	//wp_enqueue_style( 'bootstrap' );
	wp_enqueue_style( 'font-awesome' );
	wp_enqueue_style( 'cyborg' );
	wp_enqueue_style( 'stylesheet', get_stylesheet_uri(), array(), '1', 'all' );

	wp_enqueue_script( 'jquery' );
	wp_enqueue_script( 'bootstrap' );
}
add_action( 'wp_enqueue_scripts', 'sams_scripts' );


//function sm_admin() {
//	if (is_admin){
//		include('admin/admin-functions.php');
//	}
//}
//add_action( 'admin_init', 'sm_admin');

// Register Navigation Menus
        register_nav_menus(
            array(
	'main_menu' => 'Main Menu',
	'footer_menu' => 'Footer Menu',
	'left_side' => 'Left Side Menu',
	'right_side' => 'Right Side Menu'
            )
        );
// Register Sidebar
function samstrap_widgets() {
        register_sidebar(
            array(
            'name' => 'Right Sidebar',
            'id' => 'right-sidebar',
            'before_widget' => '<aside id="%1$s" class="widget %2$s">',
            'after_widget' => '</aside>',
            'before_title' => '<h3>',
            'after_title' => '</h3>',
        ));

        register_sidebar(
            array(
            'name' => 'Left Sidebar',
            'id' => 'left-sidebar',
            'before_widget' => '<aside id="%1$s" class="widget %2$s">',
            'after_widget' => '</aside>',
            'before_title' => '<h3>',
            'after_title' => '</h3>',
        ));
	$args = array(
		'id'            => 'side_menu',
		'name'          => 'Sidebar Menu',
		'description'   => 'Vertical Menu',
		'class'         => 'aside col-md-3',
		'before_title'  => '<li class="active"><a href="#"><i class="fa fa-home"></i>',
		'after_title'   => '</a></li>',
		'before_widget' => '<li class="nav-header"> <a href="#" data-toggle="collapse" data-target="#userMenu">     User <i class="fa fa-angle-down"></i>   </a>      <ul style="list-style: none;" class="collapse in" id="userMenu">',
		'after_widget'  => '</li>',
	);
	register_sidebar( $args );

}
add_action( 'widgets_init', 'samstrap_widgets' );

add_action( 'samstrap_main_content_width_hook', 'samstrap_main_content_width_columns');

function samstrap_main_content_width_columns () {

    global $samstrap_settings;

    $columns = '12';

    if ($samstrap_settings['right_sidebar'] != 0) {
        $columns = $columns - $samstrap_settings['right_sidebar_width'];
    }

    if ($samstrap_settings['left_sidebar'] != 0) {
        $columns = $columns - $samstrap_settings['left_sidebar_width'];
    }

    echo $columns;
}

function samstrap_main_content_width() {
    do_action('samstrap_main_content_width_hook');
}

////////////////////////////////////////////////////////////////////
// Add support for a featured image and the size
////////////////////////////////////////////////////////////////////

    add_theme_support( 'post-thumbnails' );
    set_post_thumbnail_size(300,300, true);

    require_once('Includes/wp_bootstrap_navwalker.php');
    require_once('Includes/bootstrap-custom-menu-widget.php');
////////////////////////////////////////////////////////////////////
// Set Content Width
////////////////////////////////////////////////////////////////////

if ( ! isset( $content_width ) ){ $content_width = 1130;}




/************************************************  WORDPRESS WHITELABEING GOODNESS  ****************************************/ 

/**REBRAND WP LOGIN STYLES**/
function my_custom_login() { 
echo '<link rel="stylesheet" type="text/css" href="' . get_bloginfo('template_directory') . '/path/to/custom_admin.css" />';
}
add_action('login_head', 'my_custom_login');


/**CUSTOM LOGIN URL**/
function new_wp_login_url() {
    echo bloginfo('url');
}
add_filter('login_headerurl', 'new_wp_login_url');


/**CUSTOM LOGIN SCREEN TITLE**/
function new_wp_login_title() {
    echo 'Powered by Repair Shop Marketing Tools';
}
add_filter('login_headertitle', 'new_wp_login_title');



/**REPLACE WP LOGO**/
function custom_admin_css() {
echo '<link rel="stylesheet" id="custom_admin" type="text/css" href="' . get_bloginfo('template_directory') . '/custom/custom_admin.css" />';
}

add_action('admin_head','custom_admin_css');
/**END REPLACE WP LOGO**/


/**REPLACE FOOTER TEXT**/
/**REPLACE THE # IN THE HREF WITH SUPPORT LINK**/
function filter_footer_admin() { ?>
Support through <a href="#">Repair Shop Marketing Tools</a>
<?php }

add_filter('admin_footer_text', 'filter_footer_admin');
/**END REPLACE FOOTER TEXT**/


/**REPLACE HOWDY**/
/** THIS IS A BIT HEAVY FOR WHAT IT DOES IDK ABOUT THIS ONE**/
// Customize:
$nohowdy = "Welcome Back";

// Hook in
if (is_admin()) {
add_action('admin_footer', 'ozh_nohowdy_f');
}

// Modify
function ozh_nohowdy_f() {
global $nohowdy;
echo <<<JS
<script type="text/javascript">
//<![CDATA[
var nohowdy = "$nohowdy";
jQuery('#user_info p')
.html(
jQuery('#user_info p')
.html()
.replace(/Howdy/,nohowdy)
);
//]]>
JS;
}
/**END REPLACE HOWDY**/


/** REMOVE DASHBOARD WIDGETS **/
function remove_dashboard_widgets() {
    $user = wp_get_current_user();
    if ( ! $user->has_cap( 'manage_options' ) ) {
        remove_meta_box( 'dashboard_recent_comments', 'dashboard', 'normal' );
        remove_meta_box( 'dashboard_incoming_links', 'dashboard', 'normal' );
        remove_meta_box( 'dashboard_quick_press', 'dashboard', 'side' );
        remove_meta_box( 'dashboard_primary', 'dashboard', 'side' );
        remove_meta_box( 'dashboard_secondary', 'dashboard', 'normal' );
        remove_meta_box( 'dashboard_browser_nag', 'dashboard', 'normal');
        remove_meta_box( 'dashboard_right_now', 'dashboard', 'normal');
        remove_meta_box( 'dashboard_activity', 'dashboard', 'normal');
    }
}
add_action( 'wp_dashboard_setup', 'remove_dashboard_widgets' );
global $wp_meta_boxes;

//REMOVE DASHBOARD WELCOME MESSAGE//
remove_action( 'welcome_panel', 'wp_welcome_panel' );

/** INSERT CUSTOM DASHBOARD WIDGETS **/
function tutorial_dashboard() {
?>
<h4>Adding Images</h4>
<p>From the Post or Page Edit Screen look for the Icons to the right of "Upload/Insert."  Click on the first Icon and follow the instructions on screen</p>
<h4>Adding Gallery Images</h4>
<p><a href="#">Adding Gallery Video Tutorial</a>
<h4>Cheat Sheats</h4>
<p><a href="#">Download</a> a Hard Copy of the WordPress Cheat Sheat</p>
<?php }
 
function tutorial_dashboard_setup() {
wp_add_dashboard_widget( 'tutorial_dashboard', __( 'Tutorial Dashboard Title' ), 'tutorial_dashboard' );
}

add_action('wp_dashboard_setup', 'tutorial_dashboard_setup');
/** END INSERT CUSTOM DASHBOARD WIDGETS **/

function remove_head_junk(){
	remove_action('wp_head', 'rsd_link');
	remove_action('wp_head', 'wp_generator');
	remove_action('wp_head', 'wlwmanifest_link');
}
add_filter( 'remove_head_clutter', 'remove_head_junk' );


// CUSTOM DASHBOARD WIDGET SHORTCODE //
function my_custom_dashboard_widgets() {
global $wp_meta_boxes;
 
wp_add_dashboard_widget('custom_help_widget', 'My Widget Title', 'custom_dashboard_help');
}
 
function custom_dashboard_help() {
echo 'My widget content';
}
 
add_action('wp_dashboard_setup', 'my_custom_dashboard_widgets');
//REMOVE DASHBOARD WELCOME MESSAGE//
remove_action( 'welcome_panel', 'wp_welcome_panel' );


//REMOVE SCREEN OPTIONS TAB//
function remove_screen_options_tab()
{
    return false;
}
add_filter('screen_options_show_screen', 'remove_screen_options_tab');

// REMOVE ADMIN BAR LOGO //
function admin_bar_remove() {
        global $wp_admin_bar;
        $wp_admin_bar->remove_menu('wp-logo');
}



// DISABLE EMPTY SIDEBAR //
if( is_active_sidebar('sidebar-name') ) { ?>

  <ul class="widgets-container">
    
<?php dynamic_sidebar('sidebar-name'); ?>

  </ul><!-- /widgets-container -->
<?php }


// USER LEVEL CHECKS //
if( current_user_can('editor') ) {
    // true if user is an editor
}

if( !current_user_can('administrator') ) {
    // true if user is not admin
}

if( current_user_can('edit_posts') ) {
    // true if user can edit posts
}

// MODIFY USER ROLE //
# Get the user role #
$edit_contributor = get_role('contributor');
# Contributor can upload media #
$edit_contributor->add_cap('upload_files');
# Contributor can no longer delete posts #
$edit_contributor->remove_cap('delete_posts');


// REMOVE UPDATE NAG //
add_filter( 'pre_site_transient_update_core', create_function( '$a', "return null;" ) );

// Disable automatic formatting
remove_filter('the_content', 'wptexturize');
remove_filter('the_excerpt', 'wptexturize');


// ENABLE LINK MANAGER //
add_filter( 'pre_option_link_manager_enabled', '__return_true' );