<div <?php post_class(); ?>>

<h2 class="page-header"><?php the_title() ;?></h2>

<?php if ( has_post_thumbnail() ) : ?>
<?php the_post_thumbnail(); ?>
<div class="clear"></div>
<?php endif; ?>
<?php the_content(); ?>
<?php wp_link_pages(); ?>
<?php get_template_part('postmeta'); ?>
<?php comments_template(); ?>

</div>