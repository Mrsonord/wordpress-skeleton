    <div role="contentinfo" class="ssrap-footer">
        <?php
            global $samstrap_settings;
            if ($samstrap_settings['author_credits'] != 0) : ?>
                <div class="row ssrap-author-credits">
                    <p class="text-center"><a href="<?php global $developer_uri; echo esc_url($developer_uri); ?>">Samstrap <?php _e('created by','samstrap') ?> Sam Nord</a></p>
                </div>
        <?php endif; ?>

        <?php get_template_part('footernav'); ?>
    </div>

</div>
<!-- end main container -->

<?php wp_footer(); ?>
</body>
</html>