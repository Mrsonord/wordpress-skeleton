<?php
    global $samstrap_settings;
    if ($samstrap_settings['right_sidebar'] != 0) : ?>
    <div class="col-md-<?php echo $samstrap_settings['right_sidebar_width']; ?> ssrap-right">
        <?php //get the right sidebar
        dynamic_sidebar( 'Right Sidebar' ); ?>
    </div>
<?php endif; ?>