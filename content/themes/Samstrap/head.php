<?php global $samstrap_settings; ?>

<div class="container ssrap-container">

<?php if ($samstrap_settings['show_header'] != 0) : ?>

    <div role="banner" class="row ssrap-header">

        <?php if ( get_header_image() != '' || get_header_textcolor() != 'blank') : ?>

        <?php if ( get_header_image() != '' ) : ?>
            <div class="col-md-4 ssrap-header-img text-center">
                <a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img width="175" height="108" title="" alt="" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAK8AAABsCAMAAAArMBJdAAABBVBMVEUAAABVVVWAgID///9VVVVVVVVVVVUAAADilyJVVVUAAADilyJVVVVVVVVVVVVVVVVVVVUAAADilyJVVVVVVVVVVVUAAADilyJVVVUAAADilyIAAADilyJVVVVVVVVVVVVVVVUAAADilyJVVVVVVVVVVVUAAADilyJVVVVVVVUAAABVVVXilyJ/VRNVVVVVVVUAAADilyJVVVVVVVVVVVUAAADilyJVVVVVVVVVVVVVVVUAAABVVVXilyJVVVVVVVVVVVUhFgVVVVUAAADilyJVVVVVVVVVVVUwIAdVVVUAAADilyJVVVVVVVVVVVVVVVVVVVUAAAAtHgdLMgtVVVV5URLilyKkpWiCAAAAUXRSTlMAAAAAAwYJEREbIiIkJyotMDMzPD9CRERFVVVmZmxvcnV3d3h7hIiIk5aZmZmbpaiqqquusbu7wMPGyczMzM/S1dvb3d3e5Oft7e7u8PP5/P6syidBAAAH1UlEQVR42u2aC3ebxhKAl8ouBPm2BaeiEiQtNJFbRbeV3CKnl5C0RbUbcO3svSr//6d0Znd5S46FQUbnao6PBcvuzMfsPNYP0tsvIQfeA++Bd9e8n2whknT87OLm5uLZsSR9Ukt2zPvk7YrJ2ydS7iWe/nx5+fPTe73CbnmP362EvDvOeL/lQ992j/fZKpVn6eDTWz5ye9o53tcZ7+vq4EXneG8y3pt08DoZuu4y719V3r86x/tLxvvLxniQjr5+c/vm6yNpx7ySJH1+frm6PP9cSkrV84z3+cZ8O7rg9Ee75j1+wff/5kXiq3vVsxdiyotd836XOvM7KekXAvhdqV9cXWX94g+x6I8d855mm786Tfvxc9jt18/v6sc3lQqyG94fcrw/bJGUj+Xfyxzv5Ra8L8WalzvmzRXb9Xu7QT7lVe8/n+6Jf6Wjb96u3n6z8/pbN34fq7+tqQ+d5pVepbivpNqA0pMfLy9/TMt1q/3tJe+0ty+P6/P+6z2qeP/ZTs4PX5xfra7Ov5Ck+rznfId+epTzWQ25Lp41/095JZSvXr25Xb159dVDtr8iP7USD4B4cpGeu08a5P2slXyTpNP3WbV9f9ocr/Tk/OrqvOl6Jp3kcAH4pK1wbor3YlWQi47zfrkqyZfd5v2+zPt9t3l/L/P+3m3emzLvTbd5r/eHl7W2XyvxIDUtXG9DvNV86zZvtZ51m7fSL6SO854UMu76pOu8xfPOU6kV3g8fPjTGK52kv8d93YJ3E2mOl53XV+K8/lCEnfA27a1kxgM5m+J9pL/H7gfsg3gf9e/de0Jbk/fx/59gX3Br8Hbi/zX2hHZr3l6XeJOjQ3dp87z5o+RGXGI4CiFEMQz8qCeG4+jsYrQYbbGsxFs8qm9yrhLHjn62jEHO7mFDn1vlISfCxVO8jOKoNm/5R6ENsQC8zB6K9XEbi9gvDigLULCI+Nv68aI1XhE+FnAuzyAYjPg+xiqTANcBatggdlObd7W6GzgJ94XYSkSZftzGMi5GqCN2xZg7rfKm6ekw/+Qs3y2KXryPCoSt8ebKieAdRWlkKkafuaxQMAzLSe/1fjpafMkib99x1iiqw9sr8xoQFkuuVpnyUjGNoz6xIIMUHMOc9DG7IoucxXEfLkZ8UwwOzqCQV5/Hc6ZnLlI4r6geb6/IO3WwnE0Frs9KhUPAHEMHBACMpnCv86QESD3iiceiSXd8gQa8Br6ZzvVA2YiUvKJ6vL0Sb4w4erqlkQEu8RlKvByhtTn6xkBXYrXGJxA8rCDocU4MWLyM+BW+o8HDJaeoFm+vypt1Cos5B+ygGdzIEdjtY+gyXiJ4Y8tivOw6nlts3hkWt8hgE/s8LZYwmFNUh7dX4Y1ySeNDUTPOIviOZkYJJnhymueFIFg4IjmNrDIvcAlbcYaRwAO6pGjLftGr8jrTFBj2F2M5cpjnMODALZDk+kJss+Dd0ElYfWBgPi/mgjdTtC1vbx0vxKsowtjtormlZKnP4GB44dzNywIg4xUKfb5RqaKtzzvreRF4ysNxqWedK/NQNCrGb5XXwk1PeXWx+SLf1veSe5wnext4E2Ch1OFpItxmsBycruNVfNFklChekoxXBKuFMZAp+ghv9bze28grgDkvnF7mwgwM9xc4w1obvw57FaDxWU7leUdMz5TkFN3Fe+/jffLeCGxNMa3RuI5mFKR0FAR1MA+rvMAFzUxxRInJx+8UOyXUsUwRaYKXWxTAUNjj5YL3KnHpKzBjuYz9EbpMx9lT4VSeULGPCyKLn96WIpJhjh8xJ2eKGuHVs24BHchijdPXk07Amr7PPh203sfZ8xwvb9/RmSJOaz5bCBNZWTRIQVEj8ZAdthQHnKQnP4uhWWfEx9mQgd/6/fT8JmTkOGkf6Bvp8Q0WjUhJUSO8m2R9V6ghGxUdePeVV7b5pz3Iz5Bnw/IiWa7oUf/99/+GhVVm67w25aDULcDRcXmRNyveD9yAUvpfs7BqsgNe5mC1yEuonSOjw9IIynBme7+V4iGd0yzvIndetWnAPyq82jDlNW2TaFU97p+lg29LvI4/VzJeClsqh8irTsJQI7PhwJXBsusRYgbBYOxSD6J7NpSDiRbgaw3GMxbM7m+gaOCGQCmPA09FXtn24L5Z3rzYdAIIduC6RKOTAVikHqUQJXYoEzO0J8FsgiGuQkRTLwwoIZNwxoMe1pAhtcdUJZ43DCfAKwPt0GyVV6UDOTTBtjxkO0pDDXhnELRyaMt2CPEwIMkTm8JLaMOMN5hAmtkmII9DmAPqVLtdXuJOwJdoG+CQakxkQjE+TBpQV0t50fkwPRgnRQXWsGfUxtXwCHnHoUfNVnnN0BujbdlzNeTFpIGg0MgkMFWWbxC9A/4ESOGBl/LCcgiVIYbwxMN4mM0G8NEqr0wBAWy7M/BowosuY17zkHdIhwkvLNBogRd2BxMtZBM0fD+tVV7iBmhbowPTE7wAoIb2LJAh48D+mOVSwqt6k5TXpIMBTAlceQL5ifEgD0O3zfgNIVOgdXmuGlIP4kHD1iaHGKRmSHGjVagXBPDhC6eHWEYY7wz8j8UEqwSsxVUaLBnLLfLm26lavMWt5ecEtTRN8PJuwkqxmraTQfP94mGiyuB6eYvf/z4yr+vZ4ZjsD682cU3SBm9X/l544D3wHngPvAfeA++d8g9kmUOFVXJAYwAAAABJRU5ErkJggg==" /></a>
            </div>
        <?php endif; ?>

        <div class="col-md-8 ssrap-header-text">
            <?php if ( get_header_textcolor() != 'blank' ) : ?>
                <h1><a class="custom-header-text-color" href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php bloginfo( 'name' ); ?></a></h1>
                <h4 class="custom-header-text-color"><?php bloginfo( 'description' ); ?></h4>
            <?php endif; ?>
            <?php else : ?>
                <h1><a href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php bloginfo( 'name' ); ?></a></h1>
                <h4><?php bloginfo( 'description' ); ?></h4>
            <?php endif; ?>
        </div>

    </div>

<?php endif; ?>