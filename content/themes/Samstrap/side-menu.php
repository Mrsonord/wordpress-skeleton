<?php
	if ( has_nav_menu( 'left_side' ) ) {
            echo(
    '<div class="col-md-2 col-lg-1 col-xs-3">
      <nav class="navbar navbar-inverse" role="navigation">
     <li class="nav-header"> <a href="#" data-toggle="collapse" data-target="#leftMenu">
    User <i class="fa fa-angle-down"></i>
  </a>
    <ul style="list-style:none" class="collapse in" id="leftMenu">'); ?>
  <?php wp_nav_menu( array(
                        'theme_location'    => 'left_side',
                        'depth'             => 1,
                        'container'         => 'li',
                        'container_class'   => 'nav-header',
                        'menu_class'        => 'nav navbar-nav',
                        'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
                        'walker'            => new wp_bootstrap_navwalker())
                );

            echo('</div>
</div>');
  }